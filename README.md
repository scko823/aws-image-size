# AWS SAM image resizing demo

A simple way to resize images from one AWS S3 bucket and save to another.

## Background

### AWS SAM

AWS SAM (Serverless Application Model) is used to bootstrap this demo. Learn about AWS SAM [here](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/what-is-sam.html)

## Architecture

### Overview

The project is based on a [event driven architecture](https://en.wikipedia.org/wiki/Event-driven_architecture), which led to serverless/FaaS approach as the solution.

### Architecture principles

1. Infrastructure as Code
    - using AWS SAM, which build on top of the [AWS Cloudformation](https://aws.amazon.com/cloudformation/), the complete stack with its runtime env and configurations are completely defined in the [template.yaml](./template.yaml), which give a repeatable production environment.
2. Cost saving
    - Being one of the cheapest compute service, using AWS lambda reduces the cost; handing the operation to an AWS managed service also free up developer time
3. Decoupled components
    - Using two buckets and AWS native S3 events as a trigger resize service, there is a unified way for the resize operation to be trigger; while opening lots of way to trigger the event of receiving an image
4. Automated CI/CD
    - automatically depoly code to AWS, reduce manual effort.

### Architecture Diagram

![Architecture Diagram](template-diagram.png 'Architecture Diagram')

### Future Improvement

1. Parameterized arguments
   allowed format of files should be passed into as [Cloudformation parameters](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/parameters-section-structure.html)

    ```yaml
    # template.yaml
    Parameters:
        AllowedFormats:
            Type: String
            Default: 'jpg,png'
            AllowedValues:
                - jpg
                - png
                - tiff
                - jpg,png
                # - ....

    Resources:
        MyFunc:
            Environment:
                Variables:
                    ALLOW_FORMATS: !Ref AllowedFormats
    ```


    ```

    ```js
        const ALLOW_FORMAT = process.env.ALLOW_FORMATS.split(',')
    ```

2. Add API Key to the API Gateway to secure (and profit?) from this. There is already are couple of issues and PRs for this [issue 1](https://github.com/awslabs/serverless-application-model/issues/802) [issue 2](https://github.com/awslabs/serverless-application-model/issues/547)

### Current limition

1. only image format of jpg or png supported.

## Reference

https://www.npmjs.com/package/parse-multipart
https://www.youtube.com/watch?v=BrYJlR0yRnw
