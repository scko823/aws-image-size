const fs = require("fs");
const os = require("os");
const aws = require("aws-sdk");
const im = require("imagemagick");
const multipart = require('parse-multipart');

const s3 = new aws.S3({
    apiVersion: "2006-03-01"
});

const imageContentType = ["image/jpeg", "image/png"]; // content type accepted
const imageFileExtenstions = ['jpeg', 'png']; // file extension accepted
const FACTORS = [1.0, 0.5]; // a list of factors that resize the images

/**
 *
 * @param {string} fileName filename of an incoming image
 * given a fileName, remove any spaces or . (except the last one)
 * lg.1234 Main St.jpg => lg-1234-Main-St.jpg
 */
const sanitizeFileName = (fileName) => {
    const fileNameFragments = fileName.split('.');
    const extension = fileNameFragments[fileNameFragments.length - 1];
    const dotSeperatedName = fileNameFragments.slice(0, fileNameFragments.length - 1);
    const newName = dotSeperatedName.map(fragment => {
        return fragment.split('').map(character => {
            return character === ' ' ? '-' : character
        }).join('')
    }).join('-')

    return [newName, extension].join('.')
}

/**
 *
 * @param {string} fileName given a fileName or s3 object key name
 * given a fileName or s3 object key name, is this a possible image
 */
const isFileExtensionImage = (fileName, contentType) => {
    console.log(`at isFileExtensionImage`)
    console.log(`fileName is ${fileName}`)
    console.log(`contentType is ${contentType}`)
    const fileNameFragments = fileName.split('.');
    console.log(`fileNameFragments is ${JSON.stringify(fileNameFragments, null, 4)}`)
    return imageContentType.indexOf(contentType) > -1 || imageFileExtenstions.indexOf(fileNameFragments[fileNameFragments.length - 1]) > -1

}

/**
 *
 * @param {string} fullPath the full fs dir
 * given a full dir, create this directory if doesnt exists
 */
const makeTempDirIfNotExists = (fullPath) => {
    if (!fs.existsSync(fullPath)) {
        fs.mkdirSync(fullPath);
    }
}

/**
 *
 * @param {string} originalFileName the file name of the original image
 * @param {number} factor a factor (0< x <= 1) to reduce the image w/h
 * @returns {string} the new file key for the new object in new S3 Bucket
 */
const newKeyName = (originalFileName, factor) => {
    const pathPrefix = `/tmp/${factor * 100}`;
    makeTempDirIfNotExists(pathPrefix); // side effect :(
    return `${factor * 100}/${originalFileName}`
}

function resizeAndUploadImage(originalFileData, key, width, height, factor) {
    const newFileName = newKeyName(key, factor);
    const filePathResized = `${os.tmpdir}/${newFileName}`;
    return new Promise((resolve, reject) => {
        const newWidth = Math.floor(width * factor);
        const newHeight = Math.floor(height * factor);
        im.resize({
                width: newWidth,
                height: newHeight,
                srcData: originalFileData,
                dstPath: filePathResized
            },
            (err, output) => {
                if (err) {
                    console.log(`got err err is ${err}`);
                    reject(err);
                    return;
                }
                console.log(`output is ${JSON.stringify(output, null, 4)}`);
                resolve(output);
            }
        );
    }).then(() => {
        console.log("TRYING TO PUT OBJECT");
        return s3
            .putObject({
                Bucket: process.env.DEST_BUCKET,
                Key: newFileName,
                Body: fs.readFileSync(filePathResized),
                ACL: 'public-read'
            })
            .promise();
    });
}

exports.resizeHandler = async event => {
    // get the object's bucket and key
    const {
        Records
    } = event;
    console.log(`Records.length is ${Records.length}`);
    console.log(`from s3 bucket: ${JSON.stringify(Records[0].s3, null, 4)}`);
    const bucket = Records[0].s3.bucket.name;
    const {
        key
    } = Records[0].s3.object;
    console.log(`s3 bucket name is ${bucket}`);
    console.log(`s3 object key is ${key}`);
    const s3ParamsOriginal = {
        Bucket: bucket,
        Key: key
    };

    //check the object is supported format
    const objectInfo = await s3.headObject(s3ParamsOriginal).promise();
    console.log(JSON.stringify(objectInfo, null, 4));
    if (!isFileExtensionImage(key, objectInfo.ContentType)) {
        // not an image
        console.log("not an image.")
        console.log(`objectInfo.ContentType is ${objectInfo.ContentType}`)
        console.log(`key is ${key}`)
        return;
    }
    // setting up file path to save original
    const filePathOriginal = `${os.tmpdir}/${key}`;

    // save the image
    const s3Data = await s3.getObject(s3ParamsOriginal).promise();
    fs.writeFileSync(filePathOriginal, s3Data.Body.toString("base64"), {
        encoding: "base64"
    });

    //get the original image width/height using im
    const {
        width: widthOriginal,
        height: heightOriginal
    } = await new Promise(
        (resolve, reject) => {
            im.identify(filePathOriginal, (err, features) => {
                if (err) {
                    console.log(err);
                    reject(err);
                }
                console.log(JSON.stringify(features, null, 4));
                resolve({
                    width: features.width,
                    height: features.height
                });
            });
        }
    );

    // Do the actual resizing
    await Promise.all(
        FACTORS.map(factor =>
            resizeAndUploadImage(
                s3Data.Body,
                key,
                widthOriginal,
                heightOriginal,
                factor
            )
        )
    );
};


exports.uploadHandler = async (event, context, callback) => {
    const response = {
        statusCode: 200
    }
    try {
        console.log("at uploadHandler")

        // get the boundary of the multipart form
        const contentType = event.headers['content-type'];
        console.log(`contentType is ${contentType}`)
        const boundary = contentType.replace(/^.*boundary=/, '');
        console.log(`boundary is ${boundary}`)

        // if there is no body, bail
        const body = event.body;
        if (!body) {
            console.log("no body")
            console.log(JSON.stringify(Object.keys(event), null, 4))
            // do some error handling?
            return response;
        }
        // parse the form body
        const bodyBuffer = new Buffer(body.toString(), 'base64')
        const parts = multipart.Parse(bodyBuffer, boundary);
        console.log("got parts")
        console.log(`is parts Array? ${Array.isArray(parts)}`)
        console.log(`parts.length ${parts.length}}`)
        // filter out non images
        const images = parts.filter(part => {
            console.log(`part keys are ${JSON.stringify(Object.keys(part))}`)
            console.log(`part.type is ${part.type}`)
            return isFileExtensionImage(part.filename, part.type);
        });

        if (!images.length) {
            return {
                statusCode: 400,
                body: JSON.stringify({
                    message: "no supported images"
                })
            }
        }
        const sanitizeImages = images.map(part => {
            return {
                ...part,
                filename: sanitizeFileName(part.filename)
            }
        });

        // put the images to S3 buckets
        // TODO: extract this into a util func?
        await Promise.all(sanitizeImages.map(part => {
            console.log("inside promise mapping")
            console.log(`part.filename is ${part.filename}`)
            const s3Params = {
                Bucket: process.env.SRC_BUCKET,
                Key: part.filename,
                Body: part.data,
                ContentType: part.type
            };
            return s3.putObject(s3Params).promise()
        }));

        //generate the resized images urls
        const s3ObjectKeys = sanitizeImages.map(p => {
            return p.filename
        })
        const listOfFilePath = s3ObjectKeys.map(key => {
            return FACTORS.map(factor => {
                return `${newKeyName(key, factor)}`
            })
        }).reduce((acc, fileKeys) => {
            return [...acc, ...fileKeys.map(key => {
                return `http://${process.env.DEST_BUCKET}.s3.amazonaws.com/${key}`
            })]
        }, [])
        // prep for the response
        response.body = JSON.stringify({
            uri: listOfFilePath,
            async: true
        })
        response.headers = {
            'Content-Type': 'application/json'
        };
        console.log(`response is ${JSON.stringify(response)}`);
        return response;
    } catch (err) {
        console.log('something went wrong')
        console.log(err)
        return {
            statusCode: 500
        }
    }
}

exports.serveIndexHTML = async () => {
    console.log("at serve index html")
    const response = {
        statusCode: 200,
        headers: {
            'Content-Type': 'text/html; charset=utf-8'
        },
        body: `
        <html lang="en">
            <head>
                <title>
                    Upload Image
                </title>
            </head>
            <body>
                <h1>Upload image</h1>
                <div>
                    <form enctype="multipart/form-data" action='${process.env.API}' method='POST'>
                        <input type="file" name="image" />
                        <button type="submit">submit</button>
                    </form>
                </div>
            </body>
        </html>
        `
    }
    return response;
}
